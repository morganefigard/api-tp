var express = require('express');
var router = express.Router();
var _ = require('lodash');
const axios = require('axios');

// Create RAW data array
let movies = [{
    /*movie: "Titanic",
    id: "0"*/
    id: String,
    movie: String,
    yearOfRelease: Number,
    duration: String, // en minutes
    actors: [String, String],
    poster: String, // lien vers une image d'affiche
    boxOffice: String, // en USD$
    rottenTomatoesScore: String
}];

let id_int = 0;

/* GET movies listing */
router.get('/', (req, res) => {
    // Get list of movie and return JSON
    res.status(200).json({ movies });
});

/* PUT new movie */
router.put('/', (req, res) => {
    /*
    *   WITHOUT AXIOS
    // Get the data from request
    const { movie } = req.body;
    const { yearOfRelease } = req.body;
    const { duration } = req.body;
    const { actors } = req.body;
    const { poster } = req.body;
    const { boxOffice } = req.body;
    const { rottenTomatoesScore } = req.body;
    // Create new unique id
    const id = _.uniqueId();
    // Insert it in array
    movies.push({ id, movie, yearOfRelease, duration, actors, poster, boxOffice, rottenTomatoesScore });
    *
    */

    /* WITH AXIOS */
    // Get the data from request
    const { title } = req.body;
    // Create new unique id
    //const id = _.uniqueId();
    const id = id_int.toString();
    // Make a request using Axios
    axios.get(`http://www.omdbapi.com/?t=${title}&apikey=6a3fffa`)
        .then(function (response) {
            console.log('axios');
            // handle success
            console.log(response.data.Title);
            // Get infos of movie
            const movie = response.data.Title;
            const yearOfRelease = response.data.Year;
            const duration = response.data.Runtime;
            const actors = response.data.Actors;
            const poster = response.data.Poster;
            const boxOffice = response.data.BoxOffice;
            let rottenTomatoesScore = "N/A";
            if(response.data.Ratings[1] != undefined) {
                rottenTomatoesScore = response.data.Ratings[1].Value;
            }
            // Insert it in array
            movies.push({ id, movie, yearOfRelease, duration, actors, poster, boxOffice, rottenTomatoesScore });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });

    // Return message
    res.json({
        message: `Just added ${title} with id ${id}`
    });

    id_int++;
});

/* UPDATE movie */
router.post('/:id', (req, res) => {
    // Get the :id of the movie we want to update from the params of the request
    const { id } = req.params;
    //Get the new data of the movie we want to update from the body of the request
    const { movie } = req.body;
    // Find in DB
    const movieToUpdate = _.find(movies, ["id", id]);
    // Update data with new data
    movieToUpdate.movie = movie;

    // Return message
    res.json({
        message: `Just updated ${id} with ${movie}`
    });
});

/* DELETE movie */
router.delete('/:id', (req, res) => {
    // Get the :id of the movie we want to delete from the params of the request
    const { id } = req.params;

    // Remove from DB
    _.remove(movies, ["id", id]);

    // Return message
    res.json({
        message: `Just removed ${id}`
    });
});

module.exports = router;